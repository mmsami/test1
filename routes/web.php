<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('check');
});


Route::post('/loginuser','LoginController@userCheck' );

Route::get('/user','LoginController@index' );
Route::get('/file','ModuleController@index' );
Route::get('/installhr','ModuleController@installHrModule' );
