<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Module;



class ModuleController extends Controller
{
    //
    public function index()
    {
        checkHr();

    }

    public function installHrModule()
    {
        Artisan::call('module:migrate');
        Artisan::call('module:Seed');
        Module::find(1)->update([
           'Hr'=>1
        ]);
        $name = "Hr";
        return view("install.installComplete", compact('name'));

    }
}
