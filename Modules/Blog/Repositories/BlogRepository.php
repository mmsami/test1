<?php
/**
 * Created by PhpStorm.
 * User: Codesmith 1
 * Date: 23-Jan-18
 * Time: 5:40 PM
 */
namespace Modules\Blog\Repositories;

use Modules\Blog\Entities\NewBlog as NewBlog;

class BlogRepository implements BlogInterface
{

    private $model;

    public function __construct(NewBlog $blog)
    {
        $this->model = $blog;
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
        return $this->model->all();

    }

    public function getById($id)
    {
        // TODO: Implement getById() method.

        return $this->model->find($id);
    }

    public function create(array $attributes)
    {
        // TODO: Implement create() method.

        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        // TODO: Implement update() method.

        return $this->model->find($id)->update($attributes);
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
        return $this->model->find($id)->delete();
    }
}