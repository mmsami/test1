<?php
/**
 * Created by PhpStorm.
 * User: Codesmith 1
 * Date: 23-Jan-18
 * Time: 5:38 PM
 */

namespace Modules\Blog\Repositories;

interface BlogInterface
{
    function getAll();

    function getById($id);

    function create(array $attributes);

    function update($id, array $attributes);

    function delete($id);
}