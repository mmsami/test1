<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class NewBlog extends Model
{
    protected $fillable = ['blog_slug',	'blog_description',	'creator_name'];
}
