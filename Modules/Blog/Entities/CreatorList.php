<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class CreatorList extends Model
{
    protected $fillable = ['name', 'email', 'address'];
}
