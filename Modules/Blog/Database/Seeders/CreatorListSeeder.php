<?php

namespace Modules\Blog\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Factory;
use Modules\Blog\Entities\CreatorList;

class CreatorListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = Factory::create();

        for($i = 0; $i < 10; $i++) {
            CreatorList::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'address' => $faker->address
            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
