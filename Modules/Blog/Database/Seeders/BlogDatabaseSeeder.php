<?php

namespace Modules\Blog\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Factory;

use Modules\Blog\Entities\NewBlog;

class BlogDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = Factory::create();

        for($i = 0; $i < 10; $i++) {
            NewBlog::create([
                'blog_slug' => $faker->randomDigit,
                'blog_description' => $faker->text,
                'creator_name' => $faker->name
            ]);
        }

        $this->call(CreatorListSeeder::class);


        // $this->call("OthersTableSeeder");
    }
}
