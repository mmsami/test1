@extends('layouts.app')
@section('content')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @foreach($getBlog as $get)
                            {{$get->blog_slug}}
                            {{$get->blog_description}}
                            {{$get->creator_name}}
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection
