<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Blog\Services\BlogService;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    private $blogService;

    public function __construct(BlogService $service)
    {

        $this->blogService = $service;

    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function getAllBlog()
    {
        $getBlog =  $this->blogService->getAll("creator");
        print_r($getBlog);
        //return view('blog::index', compact('getBlog'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id = null)
    {
        $individual = $this->blogService->checkIsset($id);



        return view('blog::index', compact('individual'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('blog::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function showCreators()
    {
        $getBlog = $this->creator->getAll();
        print_r($getBlog);
        //return view('blog::index', compact('getBlog'));
    }
}
