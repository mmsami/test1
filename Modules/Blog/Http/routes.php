<?php

Route::group(['middleware' => 'web', 'prefix' => 'blog', 'namespace' => 'Modules\Blog\Http\Controllers'], function()
{
    Route::get('/mod', 'BlogController@getAllBlog');
    Route::get('/create','BlogController@showCreators');
});
