<?php
/**
 * Created by PhpStorm.
 * User: Codesmith 1
 * Date: 24-Jan-18
 * Time: 3:16 PM
 */


namespace Modules\Blog\Services;

use Modules\Blog\Repositories\BlogRepository;
use Modules\Blog\Repositories\CreatorListRepository;

class BlogService{

    private $blog;
    private $creator;

    public function __construct(BlogRepository $newblog, CreatorListRepository $creatorList)
    {
        $this->blog = $newblog;
        $this->creator = $creatorList;
    }

    public function getAll($dbname = null)
    {
        return $this->$dbname->getAll();
    }


}